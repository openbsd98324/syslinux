

https://man.netbsd.org/alpha/boot.8


boot(8) - NetBSD Manual Pages
COMMAND:	SECTION:	ARCH:	COLLECTION:	 
boot
			
BOOT(8)              NetBSD/alpha System Manager's Manual              BOOT(8)


NAME

     boot -- Alpha system bootstrapping procedures


DESCRIPTION

     DEC Alpha systems can have either of two different firmware systems: ARC
     (a.k.a. AlphaBIOS), and SRM.  Some Alpha systems have both in their flash
     RAM and can switch between them on command.  ARC is used to bootstrap
     Microsoft Windows NT for Alpha.  SRM is used to bootstrap OpenVMS and
     Ultrix.  NetBSD requires SRM.

     SRM can bootstrap from supported local storage devices, e.g., IDE disks
     or CD-ROM drives, SCSI disks or CD-ROM drives, and floppy drives.  SRM
     can also network bootstrap via supported Ethernet interfaces, using BOOTP
     or MOP.  The particular capabilities of SRM will vary from system to sys-
     tem.

     When SRM boots the system, it performs a Power On Self Test (POST),
     probes the system busses to identify devices, and initializes them.  SRM
     includes an x86 instruction emulator in order to run the BIOS initializa-
     tion routines found in the PROM of any video cards found.  In this way,
     most generic PCI video cards can work in Alpha systems that have PCI bus
     slots.

     SRM then examines the state of one of several variables: auto_action.  If
     the value of auto_action is "halt" then SRM will stop, print its prompt:
     ">>>" and wait for commands to be entered on the console.  If the value
     of auto_action is "boot" then SRM will automatically bootstrap the oper-
     ating system specified by various non-volatile environment variables.

     SRM device names are not the same as in NetBSD, e.g., ewa0 is a DEC
     "tulip" Ethernet interface, dka0 is a SCSI disk on a recognized con-
     troller, dqa0 is an IDE disk on a recognized controller.  The show device
     command will list all the devices that SRM can bootstrap from.

   SRM Commands
     SRM is somewhat UNIX-like in that it incorporates a simple pipe and I/O
     redirection, which allows command sequences like:

           show config | more
           show * | more

     An essential but incomplete list of SRM commands follows:

     boot [-file filename] [-flags value] [device]

     Boot an operating system.  The default arguments for this command are
     taken from the SRM environment variables:

           boot_file     file name to bootstrap.
           boot_osflags  flags to pass to the secondary bootstrap program.
           bootdef_dev   default bootstrap device.

     help [command]

     Invoke the SRM help system.

     init

     Reset the SRM console, and take actions as specified by SRM variables.

     set variable value [-default]

     Set an SRM variable, e.g.,

           set auto_action boot
           set bootdef_dev dka0
           set ewa0_mode auto

     If the -default flag is used, the variable will be set to its default
     value.

     show variable or subsystem

     Show SRM variables and values, or show system state or configuration.  If
     a wildcard is used, then all matching SRM variables are shown, e.g.,

           show *       will display all SRM variables.
           show b*      will display all variables whose names begin with b.
           show config  will display the complete system configuration.
           show device  will display all bootable devices.
           show memory  will display the system's memory configuration.

   SRM Variables
     auto_action     What SRM will do at system startup or reset:

                     boot  automatically bootstrap the operating system.

                     halt  after POST, prompt the user on the console for com-
                           mands to execute.

                     Some Alpha systems (e.g., AlphaServer 800) have a "halt"
                     switch, which if set, will override the action of this
                     variable, and cause SRM to stop after POST and prompt the
                     user for commands to execute.

     bootdef_dev     The default bootstrap device, e.g., dka0, dqa0, ewa0.
                     The show device command will list the available and rec-
                     ognized bootable devices.

     boot_file       The file to bootstrap from; this is a null string by
                     default.

     boot_osflags    The flag passed to the secondary bootstrap program, and
                     the NetBSD kernel:

                     a     (automatic) multi-user mode bootstrap.

                     c     crash dump immediately after autoconf(4), if the
                           NetBSD kernel is compiled with DEBUG; See
                           options(4).

                     d     break into the debugger ASAP, if the NetBSD kernel
                           is compiled with DDB or KGDB; See options(4).

                     h     on a reboot request from the NetBSD kernel, halt
                           the system instead of rebooting.

                     i     the NetBSD secondary bootstrap program will stop
                           and prompt for the NetBSD kernel file name to boot-
                           strap.

                     n     the NetBSD kernel will ask for the root file sys-
                           tem's device, the kernel core dump device, and the
                           path to init(8).

                     q     bootstrap quietly.

                     s     single-user mode bootstrap.

                     v     bootstrap verbosely.

                     These may be used in combinations that are not mutually
                     exclusive.  These options are case-insensitive to be com-
                     patible with DEC operating systems.

     console         What type of console device SRM and NetBSD will use:

                     graphics  use a video card for output, and a PC keyboard
                               for input.

                     serial    use the first serial port for console.

                     Just as with Sun systems, Alpha systems will use the
                     first serial port as a console if there is no keyboard
                     plugged into the keyboard port, even if console is set to
                     "graphics".

     ew*0_mode       The media and speed for DEC "tulip" Ethernet interfaces
                     (e.g., DECchip 21040, 21140, 21143); possible values are:
                     auto (IEEE 802.3u "Nway" negotiation), BNC, AUI,
                     Twisted-Pair, FastFD (Fast Full Duplex).

     ew*0_protocols  The protocol to use when netbooting, i.e., MOP
                     (Maintenance Operations Protocol), or BOOTP (Bootstrap
                     Protocol).

                     The Alpha SRM firmware is picky about BOOTP responses;
                     the dhcpd.conf(5) on the server needs the

                           always-reply-rfc1048 on;

                     directive in the section for netbooting Alpha systems.

     os_type         This determines which system firmware will be used after
                     the next power-cycle, if both ARC and SRM are present in
                     Flash RAM.  This should be set to any of "UNIX", "osf",
                     or "vms" to select the SRM console required for NetBSD.
                     OSF refers to the Open Software Foundation.

   After bootstrap
     Once the NetBSD/alpha kernel is booted normally it initializes itself and
     proceeds to start the system.  An automatic consistency check of the file
     systems takes place, and unless this fails, the system comes up to multi-
     user operation.

     The proper way to shut the system down is with the shutdown(8) command.

     If the system crashes, it will enter the kernel debugger, ddb(4), if it
     is configured in the kernel.  If the crash occurred during initialization
     and the debugger is not present or is exited, the kernel will halt the
     system.

     If the crash occurred during normal operation and the debugger is not
     present or is exited, the system will attempt a dump to the configured
     dump device (which will be automatically recovered with savecore(8) dur-
     ing the next bootstrap cycle), and after the dump is complete (successful
     or not) the kernel will attempt a reboot.


FILES

     /boot                    NetBSD secondary bootstrap program (installed)
     /netbsd                  default NetBSD system kernel
     /usr/mdec/bootxx_cd9660  primary bootstrap for ``cd9660'' (ISO 9660) file
                              system
     /usr/mdec/bootxx_ffs     primary bootstrap for ``ffs'' file system
                              (Berkeley Fast File System)
     /usr/mdec/boot           secondary bootstrap
     /usr/mdec/netboot        network bootstrap
     /usr/mdec/ustarboot      ``ustar'' disk and tape bootstrap


SEE ALSO

     ddb(4), alpha/mkbootimage(8), alpha/setnetbootinfo(8), diskless(8),
     init(8), installboot(8), rc(8), reboot(8), savecore(8), shutdown(8)

     Alpha Architecture Reference Manual Third Edition, Digital Press, Alpha
     Architecture Committee, 1998.


BUGS

     The device names used by NetBSD/alpha and the SRM Console often have no
     relation to each other.

NetBSD 9.99                    February 17, 2017                   NetBSD 9.99
Powered by man-cgi (2021-06-01). Maintained for NetBSD by Kimmo Suominen. Based on man-cgi by Panagiotis Christias.
